#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/container/vector.hpp>
#include <iostream>
#include <vector>

using namespace std;
namespace compute = boost::compute;

int main() {
  // get default device and setup context
  auto device = compute::system::default_device();
  compute::context context(device);
  compute::command_queue queue(context, device);

  cout << "Using platform: " << device.platform().name() << endl;
  cout << "Using device: " << device.name() << endl;

  // create vector input and output
  vector<int> a = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  vector<int> b = {0, 1, 2, 0, 1, 2, 0, 1, 2, 0};
  vector<int> c(10);

  // create vectors on the device
  compute::vector<int> device_a(a.size(), context);
  compute::vector<int> device_b(b.size(), context);
  compute::vector<int> device_c(c.size(), context);

  // transfer input data to the device
  compute::copy(a.begin(), a.end(), device_a.begin(), queue);
  compute::copy(b.begin(), b.end(), device_b.begin(), queue);

  // perform computation on the device
  compute::transform(device_a.begin(), device_a.end(),
                     device_b.begin(), device_c.begin(), compute::plus<int>(), queue);

  // transfer output data from the device
  compute::copy(device_c.begin(), device_c.end(), c.begin(), queue);

  cout << "Resulting c-vector: ";
  for (auto &e : c)
    cout << e << ' ';
  cout << endl;
}
