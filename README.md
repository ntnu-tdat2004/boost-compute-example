# Small Boost.Compute example

## Prerequisites
* Linux or MacOS
* Recommended: The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp)

## Dependencies
* OpenCL
* Boost.Compute

## Compile and run
```sh
git clone https://gitlab.com/ntnu-tdat2004/boost-compute-example
juci boost-compute-example
```

Choose Compile and Run in the Project menu.
